;;; -*- no-byte-compile: t -*-
(define-package "treemacs-evil" "20190615.1013" "Evil mode integration for treemacs" '((evil "1.2.12") (treemacs "0.0")) :commit "0c54cb7a001fc573a7ff0d8b69f2c86f744980ee" :authors '(("Alexander Miller" . "alexanderm@web.de")) :maintainer '("Alexander Miller" . "alexanderm@web.de") :url "https://github.com/Alexander-Miller/treemacs")
