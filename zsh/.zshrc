# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/stan/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source "$HOME/.config/nvim/plugged/gruvbox/gruvbox_256palette.sh"


alias cl="clear"
alias v="nvim"
alias e="emacsclient -nc"
alias la="exa -a --group-directories-first"
alias lal="exa -la --group-directories-first"
alias lsl="exa -l --group-directories-first"
alias ls="exa --group-directories-first"
alias vim="nvim"
alias gs="git status"
alias gitp="git push"
alias gitam="git add . && git commit -m"

setopt complete_aliases

MNML_OK_COLOR="${MNML_OK_COLOR:-2}"
MNML_ERR_COLOR="${MNML_ERR_COLOR:-1}"

MNML_USER_CHAR="${MNML_USER_CHAR:-λ}"
MNML_INSERT_CHAR="${MNML_INSERT_CHAR:-›}"
MNML_NORMAL_CHAR="${MNML_NORMAL_CHAR:-·}"

function mnml_git {
    local statc="%{\e[0;3${MNML_OK_COLOR}m%}" # assume clean
    local bname="$(git rev-parse --abbrev-ref HEAD 2> /dev/null)"

    if [ -n "$bname" ]; then
        if [ -n "$(git status --porcelain 2> /dev/null)" ]; then
            statc="%{\e[0;3${MNML_ERR_COLOR}m%}"
        fi
        echo -n "$statc$bname%{\e[0m%}"
    fi
}

setprompt() {
setopt prompt_subst

PROMPT='%T %~
%F{red}%»%F{green}%»%F{yellow}%»%F{blue}%»%F{magenta}%»%f '

PROMPT='%T %~
%F{red}%\:%F{green}%\:%F{yellow}%\:%F{blue}%\:%F{magenta}%\:%f '


RPROMPT='$(mnml_git)'

}

setprompt

setopt menucomplete
export PATH=/home/stan/.bin:$PATH
export PATH=/home/stan/.local/bin:$PATH
export GOPATH=~/.go

#export VISUAL=nvim
#export EDITOR="$VISUAL"


bindkey ";5C" forward-word
bindkey ";5D" backward-word
bindkey "^[[3~" delete-char


#export GOROOT=/usr/local/go
export GOPATH=$HOME/.go
export PATH=$PATH:$GOPATH/bin
export TERM=xterm-256color
export ANDROID_HOME=/opt/android-sdk

export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/platform-tools
source ~/.bin/gruvbox

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

source /home/stan/.config/broot/launcher/bash/br
