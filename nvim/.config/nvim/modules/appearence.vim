colorscheme gruvbox
set t_Co=256
syntax on
set number relativenumber
set nowrap

 
hi LineNr  ctermbg=none 
hi CursorLineNR ctermbg=none ctermfg=yellow
hi Normal ctermbg=none
hi Search ctermfg=none ctermbg=238
hi SignColumn ctermbg=none ctermfg=240
hi TabLineFill ctermbg=none ctermfg=240
hi PmenuSel ctermbg=none ctermfg=250
hi TabLine ctermbg=none ctermfg=240
hi TabLineSel ctermbg=none ctermfg=245
hi Whitespace ctermfg=236 
hi EndOfBuffer ctermfg=none ctermbg=none

let $NVIM_TUI_ENABLE_TRUE_COLOR=1

set mouse=a
set wildmenu
set listchars=tab:>-,trail:· " Show tabs and trailing space

let &t_8f='[38;2;%lu;%lu;%lum'
let &t_8b='[48;2;%lu;%lu;%lum'
